#! /bin/sh

#--- configuracion

IP_BLACKLIST="184.168.221.104"
IP_WHITELIST="200.28.4.130 200.28.4.129 201.189.185.141	192.168.1.1 127.0.0.1 127.0.0.53"

TCP_PORT_ACCEPT="22 80 3306"
UDP_PORT_ACCEPT="1723"

EXT_INTERFAZ="wlp6s0b1"
INT_IP="192.168.1.150"

status() {

	iptables --table filter --list
}

stop() {

	iptables --flush
	iptables --delete-chain
	iptables -P INPUT ACCEPT
	iptables -P OUTPUT ACCEPT
	iptables -P FORWARD ACCEPT

	status
}

start() {

echo 1 > /proc/sys/net/ipv4/ip_forward

iptables -F
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP

iptables -A INPUT -p ALL -i lo -s 127.0.0.1 -j ACCEPT
iptables -A INPUT -p ALL -d $INT_IP -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -p ALL -s 127.0.0.1 -j ACCEPT
iptables -A OUTPUT -p ALL -s $INT_IP -j ACCEPT

iptables -I INPUT -p tcp --dport 80 -m state --state NEW -m recent --set
iptables -I INPUT -p tcp --dport 80 -m state --state NEW -m recent --update --seconds 20 --hitcount 10 -j DROP

iptables -A OUTPUT -o $EXT_INTERFAZ -p tcp --sport 80 -m state --state ESTABLISHED -j ACCEPT

for i in $TCP_PORT_ACCEPT
do
	iptables -A INPUT -i $EXT_INTERFAZ -p tcp --dport $i -m state --state NEW,ESTABLISHED -j ACCEPT
	iptables -A OUTPUT -o $EXT_INTERFAZ -p tcp --sport $i -m state --state ESTABLISHED -j ACCEPT
done

for i in $UDP_PORT_ACCEPT
do
	iptables -I INPUT -p udp --dport $i -j ACCEPT
done

for i in $IP_BLACKLIST
do
	iptables -A INPUT -s $i -j DROP
done
	
for i in $IP_WHITELIST
do
	iptables -A INPUT -s $i -j ACCEPT
done
	
	
status

}

case $1 in
	start)
		echo "Iniciando servicio..."
		start
            	;;
	stop) 
		echo "Deteniendo servicio..."
		stop
           	;;
	status)
		echo "Estado del servicio..."
		status
		;;
	restart)
		echo "Reiniciando servicio..."
		stop
		start
            	;;
	*)
		echo "Forma de uso: /etc/init.d/firewall.sh start|stop|status|restart"
		exit 1
		;;
esac

