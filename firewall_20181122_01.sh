#! /bin/sh

#--- configuracion

IP_BLACKLIST="184.168.221.104"
IP_WHITELIST="200.28.4.130 200.28.4.129 201.189.185.141	192.168.1.1 127.0.0.1 127.0.0.53"

TCP_PORT_ACCEPT="22 80 3306"
UDP_PORT_ACCEPT="1723"

#EXT_INTERFAZ="wlp6s0b1"
INT_IP="192.168.1.150"

status() {

	iptables --table filter --list
}

stop() {

	iptables --flush
	iptables --delete-chain
	iptables -P INPUT ACCEPT
	iptables -P OUTPUT ACCEPT
	iptables -P FORWARD ACCEPT

	status
}

start() {

echo 1 > /proc/sys/net/ipv4/ip_forward

echo "20000" > /proc/sys/net/ipv4/tcp_max_syn_backlog
echo "1" > /proc/sys/net/ipv4/tcp_synack_retries
echo "30" > /proc/sys/net/ipv4/tcp_fin_timeout
echo "5" > /proc/sys/net/ipv4/tcp_keepalive_probes
echo "15" > /proc/sys/net/ipv4/tcp_keepalive_intvl

iptables -F
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP

iptables -A INPUT -p ALL -i lo -s 127.0.0.1 -j ACCEPT

#iptables -A INPUT -p ALL -d $INT_IP -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -A INPUT -f -j LOGFRAGMENTED
iptables -A INPUT -p icmp -j DROP

iptables -A INPUT -p tcp ! --syn -m state --state NEW -j LOGTCP
iptables -A INPUT -p tcp --tcp-flags ALL ALL -j LOGTCP
iptables -A INPUT -p tcp --tcp-flags ALL NONE -j LOGTCP

iptables -A OUTPUT -p ALL -s 127.0.0.1 -j ACCEPT
iptables -A OUTPUT -p ALL -s $INT_IP -j ACCEPT

iptables -I INPUT -p tcp --dport 80 -m state --state NEW -m recent --set
iptables -I INPUT -p tcp --dport 80 -m state --state NEW -m recent --update --seconds 20 --hitcount 10 -j DROP

iptables -A OUTPUT -p tcp --sport 80 -m state --state ESTABLISHED -j ACCEPT

for i in $TCP_PORT_ACCEPT
do
	iptables -A INPUT -p tcp --dport $i -m state --state NEW,ESTABLISHED -j ACCEPT
	iptables -A OUTPUT -p tcp --sport $i -m state --state ESTABLISHED -j ACCEPT
        iptables -I OUTPUT -p tcp --dport $i -j ACCEPT
done

for i in $UDP_PORT_ACCEPT
do
	iptables -I INPUT -p udp --dport $i -j ACCEPT
	iptables -A INPUT -p udp -m multiport --dports $i -j UDPfilter
done

for i in $IP_BLACKLIST
do
	iptables -A INPUT -s $i -j DROP
done
	
for i in $IP_WHITELIST
do
	iptables -A INPUT -s $i -j ACCEPT
done
	
	
status

}

case $1 in
	start)
		echo "Iniciando servicio..."
		start
            	;;
	stop) 
		echo "Deteniendo servicio..."
		stop
           	;;
	status)
		echo "Estado del servicio..."
		status
		;;
	restart)
		echo "Reiniciando servicio..."
		stop
		start
            	;;
	*)
		echo "Forma de uso: /etc/init.d/firewall.sh start|stop|status|restart"
		exit 1
		;;
esac

